# README

- Simple project using HTML, JQuery and Bootstrap 3.0
- Projeto bem simples em HTML e JQuery utilizando Bootstrap 3.0

### What is this repository for?

- This is a simulator that helps employees to calculate the wages comparing a 8 hours/day job versus 6/hours day job.
- Página que disponibiliza cálculo do salário líquido final ao aderir à jornada de 6 Horas.
- [Heroic Features Boostrap Template](https://startbootstrap.com/template-overviews/heroic-features/)

### Who do I talk to?

- Repo owner or admin: Najib El Alam - rhafiko@gmail.com

### See it running

- [www.najibox.com.br/simulador/](http://www.najibox.com.br/simulador/)

### [Mobile ? yes, ready for Android](https://play.google.com/store/apps/details?id=br.com.najibox.simulador6horas)

![Get it on Google Play](https://bitbucket.org/rhafiko/simulador6horas/raw/master/img/google_play.png)
