/*
Base de cálculo
Parcela a deduzir do IRPF (R$)

Faixa 1 - Até 1.903,98 - 0%
Faixa 2 - De 1.903,99 até 2.826,65 - 7,5%
Faixa 3 - De 2.826,66 até 3.751,05 - 15%
Faixa 4 - De 3.751,06 até 4.664,68 - 22,5%
Faixa 5 - Acima de 4.664,68 - 27,5%

Pode-se deduzir R$ 189,59 mensais por dependente.
*/
function calcularIrpf(
  valBase,
  qtDependentes,
  valInss,
  valOutrasDeducoes,
  valPensao
) {
  var valorImposto = 0;
  var valorFaixa = 0;
  var valTetoFaixa1 = 1903.98;
  var valTetoFaixa2 = 2826.65;
  var valTetoFaixa3 = 3751.05;
  var valTetoFaixa4 = 4664.68;
  var valDependentes = 0;
  var valTotalDeducoes = 0;
  var valorBaseCalculo = 0;

  valDependentes = qtDependentes * 189.59;
  valTotalDeducoes = valInss + valOutrasDeducoes + valDependentes + valPensao;
  valorBaseCalculo = valBase - valTotalDeducoes;

  if (valorBaseCalculo > valTetoFaixa1) {
    valorFaixa = valTetoFaixa2 - valTetoFaixa1;
    valorImposto += valorFaixa * 0.075;
  }

  if (valorBaseCalculo > valTetoFaixa2) {
    valorFaixa = valTetoFaixa3 - valTetoFaixa2;
    valorImposto += valorFaixa * 0.15;
  }

  if (valorBaseCalculo > valTetoFaixa3) {
    valorFaixa = valTetoFaixa4 - valTetoFaixa3;
    valorImposto += valorFaixa * 0.225;
  }

  if (valorBaseCalculo > valTetoFaixa4) {
    valorFaixa = valorBaseCalculo - valTetoFaixa4;
    valorImposto += valorFaixa * 0.275;
  }

  return valorImposto;
  //alert("valor base calc: "+valorBaseCalculo +" - "+ "val imposto: "+valorImposto);
}

/*
Base de cálculo INSS Progressivo 
até 1.100,00	7,5%
de 1.100,01 até 2.203,48	9%
de 2.203,49 até 3.305,22	12 %
de 3.305,23 até 6.433,57	14%
*/
function calcularInss(valorBase) {
  var valorImposto = 0;
  var valorFaixa = 0;
  var valTetoFaixa1 = 1100;
  var valTetoFaixa2 = 2203.48;
  var valTetoFaixa3 = 3305.22;
  var valTetoFaixa4 = 6433.57;

  if (valorBase <= valTetoFaixa1) {
    valorImposto += roundIt(valorBase * 0.075);
  }

  if (valorBase > valTetoFaixa1) {
    valorFaixa = valTetoFaixa1 * 0.075;
    console.log("valorImposto faixa 1", valorFaixa);
    valorImposto += valorFaixa;
    var aliquota = 0.09;
    if (valorBase < valTetoFaixa2) {
      valorFaixa = (valorBase - valTetoFaixa1) * aliquota;
      valorImposto += roundIt(valorFaixa);
    } else {
      valorFaixa = (valTetoFaixa2 - valTetoFaixa1) * aliquota;
      valorImposto += roundIt(valorFaixa);
    }
    console.log("valorImposto faixa 2", roundIt(valorFaixa));
  }

  if (valorBase > valTetoFaixa2) {
    var aliquota = 0.12;
    if (valorBase < valTetoFaixa3) {
      valorFaixa = (valorBase - valTetoFaixa2) * aliquota;
      valorImposto += roundIt(valorFaixa);
    } else {
      valorFaixa = (valTetoFaixa3 - valTetoFaixa2) * aliquota;
      valorImposto += roundIt(valorFaixa);
    }
    console.log("valorImposto faixa 3", roundIt(valorFaixa));
  }

  if (valorBase > valTetoFaixa3) {
    var aliquota = 0.14;
    if (valorBase < valTetoFaixa4) {
      valorFaixa = (valorBase - valTetoFaixa3) * aliquota;
      valorImposto += roundIt(valorFaixa);
    } else {
      valorFaixa = (valTetoFaixa4 - valTetoFaixa3) * aliquota;
      valorImposto += roundIt(valorFaixa);
    }
    console.log("valorImposto faixa 4", roundIt(valorFaixa));
  }
  /*
  alert(
    "valor base calc: " + valorBase + " - " + "val imposto: " + valorImposto
  );
*/
  return valorImposto;
}

function roundIt(val) {
  return Math.round(val * 100) / 100;
}

function getMoney(str) {
  return parseInt(str.replace(/[\D]+/g, "")) / 100;
}
function formatReal(n) {
  return (
    "R$ " +
    n
      .toFixed(2)
      .replace(".", ",")
      .replace(/(\d)(?=(\d{3})+\,)/g, "$1.")
  );
}

function formatPercentual(n) {
  return (
    n
      .toFixed(2)
      .replace(".", ",")
      .replace(/(\d)(?=(\d{3})+\,)/g, "$1.") + " %"
  );
}

function calcular8Horas(
  valSalarioBase,
  optGratificacao,
  optAnos,
  valFct,
  valOutrasReceitasNaoDedutiveis,
  qtDependentes,
  valTicket,
  valCassi,
  valPrevidencia,
  valPensao,
  valOutros,
  valEmprestimo
) {
  var valGratificacao = 0;
  var valAnuenio = 0;
  var valInss = 0;
  var valorIrpf = 0;
  var salarioLiquido = 0;
  var salarioLiquidoRetorno = 0;

  valInss = calcularInss(valSalarioBase);

  valGratificacao = valSalarioBase * (optGratificacao / 100);

  valAnuenio = valSalarioBase * 0.01 * optAnos;

  var valorBaseIrpf =
    valSalarioBase +
    valGratificacao +
    valAnuenio +
    valFct +
    valOutrasReceitasNaoDedutiveis;

  //alert(valorBaseIrpf);
  valorIrpf = calcularIrpf(
    valorBaseIrpf,
    qtDependentes,
    valInss,
    valPrevidencia,
    valPensao
  );

  salarioLiquido =
    valorBaseIrpf -
    valorIrpf -
    valTicket -
    valCassi -
    valEmprestimo -
    valPensao -
    valOutros -
    valPrevidencia -
    valInss;
  salarioLiquidoRetorno = salarioLiquido;

  $("#tdValSalarioBase8").html(formatReal(valSalarioBase));
  $("#tdValAts8").html(formatReal(valAnuenio));
  $("#tdValGratificacao8").html(formatReal(valGratificacao));
  $("#tdValFct8").html(formatReal(valFct));
  $("#tdValOutrasReceitasNd8").html(formatReal(valOutrasReceitasNaoDedutiveis));
  $("#tdValSalarioBrutoIrpf8").html(formatReal(valorBaseIrpf));
  $("#tdValTicket8").html(formatReal(valTicket));
  $("#tdValConvenioMedico8").html(formatReal(valCassi));
  $("#tdValEmprestimo8").html(formatReal(valEmprestimo));
  $("#tdValInss8").html(formatReal(valInss));
  $("#tdValPrevidencia8").html(formatReal(valPrevidencia));
  $("#tdValPensao8").html(formatReal(valPensao));
  $("#tdValOutrosDescontos8").html(formatReal(valOutros));
  $("#tdValIrpf8").html(formatReal(valorIrpf));
  $("#tdValSalarioLiquido8").html(formatReal(salarioLiquido));

  return salarioLiquidoRetorno;
}

function calcular6Horas(
  valSalarioBase,
  optGratificacao,
  optAnos,
  valFct,
  valOutrasReceitasNaoDedutiveis,
  qtDependentes,
  valTicket,
  valCassi,
  valPrevidencia,
  valPensao,
  valOutros,
  valEmprestimo
) {
  valSalarioBase = valSalarioBase * 0.75;
  var valGratificacao = 0;
  var valAnuenio = 0;
  var valInss = 0;
  var valorIrpf = 0;
  var salarioLiquido = 0;
  var salarioLiquidoRetorno = 0;

  valInss = calcularInss(valSalarioBase);

  valGratificacao = valSalarioBase * (optGratificacao / 100);

  valAnuenio = valSalarioBase * 0.01 * optAnos;

  var valorBaseIrpf =
    valSalarioBase +
    valGratificacao +
    valAnuenio +
    valFct +
    valOutrasReceitasNaoDedutiveis;

  //alert(valorBaseIrpf);
  valorIrpf = calcularIrpf(
    valorBaseIrpf,
    qtDependentes,
    valInss,
    valPrevidencia,
    valPensao
  );

  salarioLiquido =
    valorBaseIrpf -
    valorIrpf -
    valTicket -
    valCassi -
    valEmprestimo -
    valPensao -
    valOutros -
    valPrevidencia -
    valInss;
  salarioLiquidoRetorno = salarioLiquido;

  $("#tdValSalarioBase6").html(formatReal(valSalarioBase));
  $("#tdValAts6").html(formatReal(valAnuenio));
  $("#tdValGratificacao6").html(formatReal(valGratificacao));
  $("#tdValFct6").html(formatReal(valFct));
  $("#tdValOutrasReceitasNd6").html(formatReal(valOutrasReceitasNaoDedutiveis));
  $("#tdValSalarioBrutoIrpf6").html(formatReal(valorBaseIrpf));
  $("#tdValTicket6").html(formatReal(valTicket));
  $("#tdValConvenioMedico6").html(formatReal(valCassi));
  $("#tdValEmprestimo6").html(formatReal(valEmprestimo));
  $("#tdValInss6").html(formatReal(valInss));
  $("#tdValPrevidencia6").html(formatReal(valPrevidencia));
  $("#tdValPensao6").html(formatReal(valPensao));
  $("#tdValOutrosDescontos6").html(formatReal(valOutros));
  $("#tdValIrpf6").html(formatReal(valorIrpf));
  $("#tdValSalarioLiquido6").html(formatReal(salarioLiquido));

  return salarioLiquidoRetorno;
}

function save(
  valSalarioBase,
  optGratificacao,
  optAnos,
  valFct,
  valFct6,
  valOutrasReceitasNaoDedutiveis,
  qtDependentes,
  valTicket,
  valCassi,
  valPrevidencia,
  valPrevidencia6,
  valPensao,
  valOutros,
  valEmprestimo
) {
  var dadosBase = {
    valSalarioBase: valSalarioBase,
    optGratificacao: optGratificacao,
    optAnos: optAnos,
    valFct: valFct,
    valFct6: valFct6,
    valOutrasReceitasNaoDedutiveis: valOutrasReceitasNaoDedutiveis,
    qtDependentes: qtDependentes,
    valTicket: valTicket,
    valCassi: valCassi,
    valPrevidencia: valPrevidencia,
    valPrevidencia6: valPrevidencia6,
    valPensao: valPensao,
    valOutros: valOutros,
    valEmprestimo: valEmprestimo,
  };
  JSONDadosBase = JSON.stringify(dadosBase);
  localStorage.setItem("dadosBase", JSON.stringify(dadosBase));
}

function calcular() {
  var valSalarioBase = $("#txSalario").val();
  var chkGratificacao = $("#chkGratificacao").is(":checked");
  var optGratificacao = $("#optGratificacao").val();
  var optAnos = $("#optAnos").val();
  var valFct = $("#txFct").val();
  var valFct6 = $("#txFct6").val();
  var valOutrasReceitasNaoDedutiveis = $(
    "#txOutrasReceitasNaoDedutiveis"
  ).val();
  var qtDependentes = $("#txDependentes").val();
  var valTicket = $("#txTicket").val();
  var valCassi = $("#txCassi").val();
  var valPrevidencia = $("#txPrevidencia").val();
  var valPrevidencia6 = $("#txPrevidencia6").val();
  var valPensao = $("#txPensao").val();
  var valOutros = $("#txOutros").val();
  var valEmprestimo = $("#txEmprestimo").val();
  var valorSalario8 = 0;
  var valorSalario6 = 0;
  var percentualReducao = 0;

  save(
    valSalarioBase,
    optGratificacao,
    optAnos,
    valFct,
    valFct6,
    valOutrasReceitasNaoDedutiveis,
    qtDependentes,
    valTicket,
    valCassi,
    valPrevidencia,
    valPrevidencia6,
    valPensao,
    valOutros,
    valEmprestimo
  );

  valSalarioBase = getMoney(valSalarioBase);
  valFct = getMoney(valFct);
  valFct6 = getMoney(valFct6);
  valOutrasReceitasNaoDedutiveis = getMoney(valOutrasReceitasNaoDedutiveis);
  valTicket = getMoney(valTicket);
  valCassi = getMoney(valCassi);
  valPrevidencia = getMoney(valPrevidencia);
  valPrevidencia6 = getMoney(valPrevidencia6);
  valPensao = getMoney(valPensao);
  valOutros = getMoney(valOutros);
  valEmprestimo = getMoney(valEmprestimo);

  valSalarioBase =
    parseFloat(valSalarioBase) > 0 ? parseFloat(valSalarioBase) : 0;
  valFct = parseFloat(valFct) > 0 ? parseFloat(valFct) : 0;
  valFct6 = parseFloat(valFct6) > 0 ? parseFloat(valFct6) : 0;
  valOutrasReceitasNaoDedutiveis =
    parseFloat(valOutrasReceitasNaoDedutiveis) > 0
      ? parseFloat(valOutrasReceitasNaoDedutiveis)
      : 0;
  valTicket = parseFloat(valTicket) > 0 ? parseFloat(valTicket) : 0;
  valCassi = parseFloat(valCassi) > 0 ? parseFloat(valCassi) : 0;
  valPrevidencia =
    parseFloat(valPrevidencia) > 0 ? parseFloat(valPrevidencia) : 0;
  valPrevidencia6 =
    parseFloat(valPrevidencia6) > 0 ? parseFloat(valPrevidencia6) : 0;
  valPensao = parseFloat(valPensao) > 0 ? parseFloat(valPensao) : 0;
  valOutros = parseFloat(valOutros) > 0 ? parseFloat(valOutros) : 0;
  valEmprestimo = parseFloat(valEmprestimo) > 0 ? parseFloat(valEmprestimo) : 0;

  valorSalario8 = calcular8Horas(
    valSalarioBase,
    optGratificacao,
    optAnos,
    valFct,
    valOutrasReceitasNaoDedutiveis,
    qtDependentes,
    valTicket,
    valCassi,
    valPrevidencia,
    valPensao,
    valOutros,
    valEmprestimo
  );

  valorSalario6 = calcular6Horas(
    valSalarioBase,
    optGratificacao,
    optAnos,
    valFct6,
    valOutrasReceitasNaoDedutiveis,
    qtDependentes,
    valTicket,
    valCassi,
    valPrevidencia6,
    valPensao,
    valOutros,
    valEmprestimo
  );

  percentualReducao =
    parseFloat(valorSalario6 / valorSalario8) > 0
      ? parseFloat((1 - valorSalario6 / valorSalario8) * 100)
      : 0;
  valorReducao =
    parseFloat(valorSalario8 - valorSalario6) > 0
      ? parseFloat(valorSalario8 - valorSalario6)
      : 0;
  $("#spnPercentualReducao").html(formatPercentual(percentualReducao));
  $("#tdValGratificacaoLabel").html("Gratificação " + optGratificacao + "%");
  $("#spnValorReducao").html(formatReal(valorReducao));
}

function readFromStorage() {
  var JSONDadosBase = localStorage.getItem("dadosBase");

  if (JSONDadosBase != null) {
    var obj = JSON.parse(JSONDadosBase);

    $("#txSalario").val(obj.valSalarioBase);
    $("#optGratificacao").val(obj.optGratificacao);
    $("#optAnos").val(obj.optAnos);
    $("#txFct").val(obj.valFct);
    $("#txFct6").val(obj.valFct6);
    $("#txOutrasReceitasNaoDedutiveis").val(obj.valOutrasReceitasNaoDedutiveis);
    $("#txDependentes").val(obj.qtDependentes);
    $("#txTicket").val(obj.valTicket);
    $("#txCassi").val(obj.valCassi);
    $("#txPrevidencia").val(obj.valPrevidencia);
    $("#txPrevidencia6").val(obj.valPrevidencia6);
    $("#txPensao").val(obj.valPensao);
    $("#txOutros").val(obj.valOutros);
    $("#txEmprestimo").val(obj.valEmprestimo);

    calcular();

    $("#txSalario").focus();
    $("#modalDataLoaded").modal();
  }
}
